#!/usr/bin/python3
#
#    Copyright 2020 Guy Streeter
#
#    This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

#    Author: Guy Streeter guy.streeter@gmail.com

import os
import sys

from typing import List

from gi import require_version as gi_require_version

gi_require_version('Gtk', '3.0')
from gi.repository import GLib, GObject, Gio, Gtk

#
# debugging tools
#
import time
_debugFilename = 'application10.log'
with open(_debugFilename, 'w') as logfile:
    print(time.asctime(), file=logfile)


def debugPrint(*args, **kwargs) -> None:
    with open(_debugFilename, 'a') as logfile:
        print(time.asctime(), *args, file=logfile, **kwargs)


import functools


def debug(func):
    '''Print the function signature and return value'''
    @functools.wraps(func)
    def wrapper_debug(*args, **kwargs):
        args_repr = [repr(a) for a in args]                      # 1
        kwargs_repr = [f'{k}={v!r}' for k, v in kwargs.items()]  # 2
        signature = ', '.join(args_repr + kwargs_repr)           # 3
        debugPrint(f'Calling {func.__name__}({signature})')
        value = func(*args, **kwargs)
        debugPrint(f'{func.__name__!r} returned {value!r}')      # 4
        return value
    return wrapper_debug


ORG_STRING = 'org.gtk.exampleapp'
ORG_SLASHES = '/'.join(ORG_STRING.split('.'))


#
# The preferences dialog
#
class _ExamplePrefs(Gtk.Dialog):
    def __init__(self, parent: Gtk.Window):
        # NOTE: other stuff depends on the Window being initialized first.
        super().__init__(
            visible=True,
            parent=parent,
            modal=True,
            resizable=False,
            title='Preferences',
            use_header_bar=True
        )
        self._settings = Gio.Settings(schema=ORG_STRING)
        grid = Gtk.Grid(
            visible=True,
            orientation='horizontal',
            margin=6,
            row_spacing=12,
            column_spacing=6
        )
        level = (
            Gtk.FontChooserLevel.FAMILY |
            Gtk.FontChooserLevel.STYLE |
            Gtk.FontChooserLevel.SIZE
        )
        # Initialize the font selector to the value in Settings.
        font = Gtk.FontButton(
            visible=True,
            show_style=True,
            show_size=True,
            level=level
        )
        fontlabel = Gtk.Label(
            visible=True,
            label='_Font:',
            use_underline=True,
            mnemonic_widget=font
        )
        grid.add(fontlabel)
        grid.add(font)
        self.get_content_area().add(grid)
        # Bind the 'font' key in Settings to the 'font' property of
        # the FontButton. The DEFAULT flag sets the button value from
        # Setting, and sets Settings if the button value changes.
        self._settings.bind(
            'font',
            font,
            'font',
            Gio.SettingsBindFlags.DEFAULT
        )


#
# The applicaton main window
#
class _ExampleAppWindow(Gtk.ApplicationWindow):
    def __init__(self, application=None):
        super().__init__(
            application=application,
            default_width=600,
            default_height=400
        )
        contentbox = Gtk.Box(
            visible=True,
            orientation='vertical'
        )
        self._stack = Gtk.Stack(visible=True)
        self._stack.connect(
            'notify::visible-child',
            self._visible_child_changed
        )
        switcher = Gtk.StackSwitcher(visible=True, stack=self._stack)
        self._header = Gtk.HeaderBar(
            visible=True,
            custom_title=switcher,
            show_close_button=True,
            has_subtitle=False
        )
        self.set_titlebar(self._header)
        lines_label = Gtk.Label(label='Lines:', visible=False)
        self._header.pack_start(lines_label)
        self._lines = Gtk.Label(visible=False)
        self._header.pack_start(self._lines)
        self._lines.bind_property('visible', lines_label, 'visible')
        gear_model = Gio.Menu()
        gear_model.append(
            label='Show _Lines',
            detailed_action="win.show-lines"
        )
        gears = Gtk.MenuButton(
            visible=True,
            direction=Gtk.ArrowType.NONE,
            menu_model=gear_model
        )
        self._header.pack_end(gears)
        action = Gio.PropertyAction(
            name='show-lines',
            object=self._lines,
            property_name='visible'
        )
        self.add_action(action)
        self._search = Gtk.ToggleButton(visible=True, sensitive=False)
        image = Gtk.Image.new_from_icon_name(
            'edit-find-symbolic',
            Gtk.IconSize.MENU
        )
        self._search.set_image(image)
        self._header.pack_end(self._search)
        self._searchbar = Gtk.SearchBar(visible=True)
        self._search.bind_property(
            'active',
            self._searchbar,
            'search-mode-enabled',
            GObject.BindingFlags.BIDIRECTIONAL
        )
        self._search_entry = Gtk.SearchEntry(visible=True)
        self._search_entry.connect(
            'search-changed',
            self._search_changed
        )
        self._searchbar.connect_entry(self._search_entry)
        self._searchbar.add(self._search_entry)
        contentbox.add(self._searchbar)
        contentbox.add(self._stack)
        self.add(contentbox)
        self._settings = Gio.Settings(schema_id=ORG_STRING)

    def open(self, gfile: Gio.File) -> None:
        basename = gfile.get_basename()
        results: List = gfile.load_contents()
        success: bool = results[0]
        if success:
            contents: bytes = results[1]
            view = Gtk.TextView(
                visible=True,
                editable=True,
                cursor_visible=True
            )
            buffer: Gtk.TextBuffer = view.get_buffer()
            try:
                buffer.set_text(contents.decode())
            # Skip binary files.
            except UnicodeDecodeError:
                return
        scrolled = Gtk.ScrolledWindow(visible=True, hexpand=True, vexpand=True)
        scrolled.add(view)
        self._stack.add_titled(scrolled, basename, basename)
        tag = buffer.create_tag()
        self._settings.bind('font', tag, 'font', Gio.SettingsBindFlags.DEFAULT)
        start_iter = buffer.get_start_iter()
        end_iter = buffer.get_end_iter()
        buffer.apply_tag(tag, start_iter, end_iter)
        self._search.set_sensitive(True)
        self._lines.set_visible(True)
        self._update_lines()

    def _visible_child_changed(self, _params, _widget) -> None:
        self._searchbar.set_search_mode(False)
        self._update_lines()

    def _search_changed(self, entry: Gtk.SearchEntry) -> None:
        view: Gtk.TextView = self._stack.get_visible_child().get_child()
        buffer: Gtk.TextBuffer = view.get_buffer()
        start: Gtk.TextIter = buffer.get_start_iter()
        result = start.forward_search(
            entry.get_text(),
            Gtk.TextSearchFlags.CASE_INSENSITIVE,
            None
        )
        if result is None:
            return
        match_start, match_end = result
        buffer.select_range(match_start, match_end)
        view.scroll_to_iter(match_start, 0.0, False, 0.0, 0.0)

    def _update_lines(self) -> None:
        tab: Gtk.Widget = self._stack.get_visible_child()
        debugPrint('ul0', tab)
        if tab is None:
            self._lines.set_visible(False)
            return
        buffer: Gtk.TextBuffer = tab.get_child().get_buffer()
        count: int = buffer.get_line_count()
        self._lines.set_text(f'{count:d}')


#
# The Application Menu
#
class _AppMenu(Gio.Menu):
    def __init__(self):
        super().__init__()
        prefs = Gio.Menu()
        # One-step 'Preferences' entry.
        prefs.append('_Preferences', 'app.preferences')
        # Put the item in it's own menu section.
        self.append_section(label=None, section=prefs)
        # Two-step for 'Quit' entry, to add an accelerator
        quit = Gio.Menu()
        # MenuItem doesn't have a constructor that accepts args, so
        # we have to use the .new class method.
        item: Gio.MenuItem = Gio.MenuItem.new('_Quit', 'app.quit')
        item.set_attribute_value('accel', GLib.Variant('s', '<Ctrl>Q'))
        quit.append_item(item)
        self.append_section(label=None, section=quit)


#
# The application
#
class _ExampleApp(Gtk.Application):
    def __init__(
        self,
        application_id=ORG_STRING,
        flags=Gio.ApplicationFlags.HANDLES_OPEN
    ):
        # Just do the super init() here. The do_startup() virtual method
        # will be called after the Application is properly initialized.
        super().__init__(application_id=application_id, flags=flags)

    def do_startup(self):
        # Do the meta startup stuff first. super() doesn't work here.
        Gtk.Application.do_startup(self)

        def app_quit(_action, _param) -> None:
            self.quit()

        quit_action = Gio.SimpleAction(name='quit')
        quit_action.connect('activate', app_quit)
        self.add_action(quit_action)

        def app_prefs(_action, _param) -> None:
            prefs = _ExamplePrefs(self.get_active_window())
            prefs.present()

        prefs_action = Gio.SimpleAction(name='preferences')
        prefs_action.connect('activate', app_prefs)
        self.add_action(prefs_action)
        self.set_app_menu(_AppMenu())

    # Called if there are no filenames on the commandline
    def do_activate(self):
        window = _ExampleAppWindow(self)
        window.present()

    # Called if there are any filenames on the commandline.
    # NOTE: do_activate() is not called in this case.
    # There might already be a window open. The application can be started
    # multiple times, and subsequent invocations will connect to the first
    # instance (by default), then call this routine if they have filenames on
    # their commandline.
    def do_open(self, files: List[Gio.File], _nfiles: int, _hint: str):
        windows: List[Gtk.window] = self.get_windows()
        try:
            window = windows[0]
        except IndexError:
            window = _ExampleAppWindow(self)
            self.add_window(window)
        window.present()
        for file in files:
            window.open(file)


#
# The main loop
#

# When this application is running un-installed, look in the current directory
# for the schema.
# This is a hack, but it is just for testing anyway. If the script is not
# somewhere below '/usr', it is un-installed.
filepath = os.path.abspath(__file__)
if not filepath.startswith('/usr/'):
    schemapath = os.path.dirname(filepath)
    os.environ['GSETTINGS_SCHEMA_DIR'] = schemapath
    debugPrint(f'Setting GSETTINGS_SCHEMA_DIR to {schemapath}')
del filepath, schemapath

thisApp = _ExampleApp()
thisApp.run(sys.argv)
