#
#    Copyright 2020 Guy Streeter
#
#    This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#    You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

#    Author: Guy Streeter guy.streeter@gmail.com

GLIB_COMPILE_SCHEMAS := $(shell pkg-config --variable=glib_compile_schemas gio-2.0 2>/dev/null)
ifndef GLIB_COMPILE_SCHEMAS
 GLIB_COMPILE_SCHEMAS := glib-compile-schemas
endif

CLEANFILES = application10.log gschemas.compiled

all: gschemas.compiled

gschemas.compiled: org.gtk.exampleapp.gschema.xml
	$(GLIB_COMPILE_SCHEMAS) .

clean:
	rm -f $(CLEANFILES)
